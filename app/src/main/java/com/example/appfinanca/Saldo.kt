package com.example.appfinanca

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.appfinanca.db.DbFinance

class Saldo : AppCompatActivity() {

    //variaveis
    lateinit var database: DbFinance
    private lateinit var context: Context

    private var gastoTotal: Double = 0.0
    private var ganhoTotal: Double = 0.0
    private var saldo: Double = 0.0

    fun setContext(context: Context){
        this.context = context
        database = DbFinance(context)
        ganhoTotal = database.saldoGanhosFromDB();
        gastoTotal = database.saldoGastosFromDB();
        saldo = ganhoTotal - gastoTotal

        print(ganhoTotal)
        print(gastoTotal)
        print(saldo)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_saldo)
        setContext(this)
        val textView: TextView = findViewById(R.id.textViewSaldo)
        val textView2: TextView = findViewById(R.id.textViewGanho)
        val textView3: TextView = findViewById(R.id.textViewGasto)
        textView.setText("R$"+ String.format("%.2f",saldo))
        textView2.setText("R$"+ String.format("%.2f",ganhoTotal))
        textView3.setText("R$"+String.format("%.2f",gastoTotal))

        //colocar na tela


/*
        val textView2: TextView = findViewById(R.id.textViewGanho) as TextView
        textView.setOnClickListener {
            textView2.text = ganhoTotal.toString()
        }
        val textView3: TextView = findViewById(R.id.textViewGastos) as TextView
        textView.setOnClickListener {
            textView3.text = gastoTotal.toString()
        }*/

    }
}